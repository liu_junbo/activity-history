# activity-history


'ActivityHistory' => \Ldawn\ActivityHistory\Middleware\ActivityHistoryMiddleware::class,
 
<li data-name="component" class="layui-nav-item">
    <a href="javascript:;" lay-tips="后台操作记录" lay-direction="2">
        <i class="layui-icon layui-icon-user"></i>
        <cite>后台操作记录</cite>
    </a>
    <dl class="layui-nav-child">
        <dd data-name="button">
            <a lay-href="{{ URL::asset('/admin/activityHistory/index') }}">后台操作记录</a>
        </dd>
    </dl>
</li>