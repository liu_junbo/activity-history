<?php

use Illuminate\Support\Facades\Route;
use Ldawn\ActivityHistory\Controllers\ActivityHistoryController;

Route::group(['prefix' => '/admin', 'middleware' => ['web','BeforeRequest', 'CheckAdminLogin','ActivityHistory']], function () {
    Route::any('activityHistory/index', [ActivityHistoryController::class, 'index']);
    Route::any('activityHistory/info', [ActivityHistoryController::class, 'info']);
});