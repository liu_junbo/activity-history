<?php

namespace  Ldawn\ActivityHistory\Controllers;

use App\Components\Common\RequestValidator;
use App\Managers\AdminManager;
use App\Models\Admin;
use Illuminate\Support\Str;
use Ldawn\ActivityHistory\Managers\ActivityHistoryManager;
use App\Components\Common\QNManager;
use App\Components\Common\UtilsFunction;
use App\Components\Common\GLogger;
use App\Components\Common\ApiResponse;
use App\Managers\ActivityUserManager;
use Ldawn\ActivityHistory\Models\ActivityHistory;
use Illuminate\Http\Request;

class ActivityHistoryController
{
    const  MODULAR='activityHistory';
    /*
    * 首页
    *
    * By ldawn
    *
    * 2021/01/09
    */
    public function index(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        //相关搜素条件
        $status = null;
        $search_word = null;
        $start_start_time = null;
        $end_start_time = null;
        $modular = null;
        $opt = null;
        if (array_key_exists('status', $data) && !UtilsFunction::isObjNull($data['status'])) {
            $status = $data['status'];
        }
        if (array_key_exists('search_word', $data) && !UtilsFunction::isObjNull($data['search_word'])) {
            $search_word = $data['search_word'];
        }
        if (array_key_exists('start_start_time', $data) && !UtilsFunction::isObjNull($data['start_start_time'])) {
            $start_start_time = $data['start_start_time'];
        }
        if (array_key_exists('end_start_time', $data) && !UtilsFunction::isObjNull($data['end_start_time'])) {
            $end_start_time = $data['end_start_time'];
        }
        if (array_key_exists('modular', $data) && !UtilsFunction::isObjNull($data['modular'])) {
            $modular = $data['modular'];
        }
        if (array_key_exists('opt', $data) && !UtilsFunction::isObjNull($data['opt'])) {
            $opt = $data['opt'];
        }
        $con_arr = array(
            'status' => $status,
            'search_word' => $search_word,
            'start_start_time' => $start_start_time,
            'end_start_time' => $end_start_time,
            'modular' => Str::camel($modular),
            'opt' => $opt,
        );

        $activity_historys =ActivityHistoryManager::getListByCon($con_arr, true);
        foreach ($activity_historys as $activity_history) {
            $activity_history = ActivityHistoryManager::getInfoByLevel($activity_history, 'admin');
        }
        $opt_list=config('activityHistory.opt_list');
        return view('ActivityHistoryView::index', ['self_admin' => $self_admin, 'datas' => $activity_historys, 'con_arr' => $con_arr, 'opt_list' =>$opt_list,'modular'=>self::MODULAR]);
    }

    /*
    * 编辑-get
    *
    * By ldawn
    *
    * 2020/11/20
    */
    public function info(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        $activity_history = new ActivityHistory();
        if (array_key_exists('id', $data)) {
            $activity_history = ActivityHistoryManager::getById($data['id']);
            $activity_history = ActivityHistoryManager::getInfoByLevel($activity_history, "admin");
            $activity_history->opt_value=UtilsFunction::jsonHtml($activity_history->opt_value);
        }
        return view('ActivityHistoryView::info', ['self_admin' => $self_admin, 'data' => $activity_history,'modular'=>self::MODULAR]);
    }
}


