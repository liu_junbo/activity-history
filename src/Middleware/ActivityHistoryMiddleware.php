<?php

/**
 * 后台行为记录中间件
 */

namespace Ldawn\ActivityHistory\Middleware;

use Closure;
use Illuminate\Support\Arr;
use Ldawn\ActivityHistory\Managers\ActivityHistoryManager;
use Ldawn\ActivityHistory\Models\ActivityHistory;

class ActivityHistoryMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $data = $request->all();
        if (!in_array($request->method(), config('ldawn.activity-history.record_method'))) {
            return $next($request);
        }
        $url_arr = explode('/', $request->path());
        $self_admin_id = null;
        if ($request->session()->has("self_admin")) {
            $self_admin_id = $request->session()->get("self_admin")->id;
        }
        $activity_history = new ActivityHistory();
        $activity_history = ActivityHistoryManager::setInfo($activity_history, [
            "admin_id" => $self_admin_id,
            "modular" => Arr::get($url_arr,1,'index'),
            "opt" =>  Arr::last($url_arr),
            "opt_value" => $data,
        ]);

        ActivityHistoryManager::save($activity_history);

        return $next($request);
    }
}
