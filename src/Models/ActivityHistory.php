<?php

/**
* Created by PhpStorm.
* User: bob
* Date: 2021/02/06
*/
namespace Ldawn\ActivityHistory\Models;

use App\Models\Common\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityHistory extends BaseModel
{
    use SoftDeletes;
    protected $table ='activity_history';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    /*
    * 进行类型转换
    *
    * @var  array
    *
    */
    protected $casts = [
            ];

}



