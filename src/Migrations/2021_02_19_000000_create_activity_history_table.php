<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivityHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('activity_history')) {
            Schema::create('activity_history', function (Blueprint $table) {
                $table->bigIncrements('id');
            });
        }
        DB::statement("alter table activity_history comment='行为记录'");
        Schema::table('activity_history', function (Blueprint $table) {
            if (!Schema::hasColumn('activity_history', 'admin_id')) {
                $table->integer('admin_id')->nullable();
            };

            if (!Schema::hasColumn('activity_history', 'modular')) {
                $table->string('modular',64);
            };

            if (!Schema::hasColumn('activity_history', 'son_modular')) {
                $table->string('son_modular',64)->nullable();
            };

            if (!Schema::hasColumn('activity_history', 'opt')) {
                $table->string('opt',64);
            };

            if (!Schema::hasColumn('activity_history', 'opt_value')) {
                $table->longText('opt_value');
            };

            if (!Schema::hasColumn('activity_history', 'status')) {
                $table->tinyInteger('status')->default(1);
            };

            if (!Schema::hasColumn('activity_history', 'seq')) {
                $table->integer('seq')->default(99);
            };

            if (!Schema::hasColumn('activity_history', 'created_at')) {
                $table->dateTime('created_at');
            };

            if (!Schema::hasColumn('activity_history', 'updated_at')) {
                $table->dateTime('updated_at');
            };

            if (!Schema::hasColumn('activity_history', 'deleted_at')) {
                $table->dateTime('deleted_at')->nullable();
            };
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_history');
    }
}
