<?php

namespace Ldawn\ActivityHistory;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\ServiceProvider;
use Ldawn\RbacA\Commands\Init;

class PackageActivityHistoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadViewsFrom(__DIR__ . '/Views', 'ActivityHistoryView');
        $this->loadMigrationsFrom(__DIR__.'/Migrations');
        $this->app->singleton('activity-history:init', Commands\Init::class);
        $this->commands([
            'activity-history:init',
        ]);
    }
}
