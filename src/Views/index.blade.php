@extends('admin.layouts.app')

@section('content')

    <style>

    </style>

    <div class="layui-fluid">
        <div class="layui-card">
            <div class="layui-form layui-card-header layuiadmin-card-header-auto">
                <form id="search_form" class="layui-form" method="post"
                      action="{{URL::asset('admin/'.$modular.'/index')}}?page={{$datas->currentPage()}}">
                    {{csrf_field()}}
                    <div class="layui-form-item">

                        @include('component.grid.time_frame',['c_title'=>'时间','c_name_1'=>'start_start_time','c_value_1'=>$con_arr['start_start_time'],'c_name_2'=>'end_start_time','c_value_2'=>$con_arr['end_start_time'],'c_type'=>'datetime'])

                        @include('component.grid.select',['c_title'=>'模块','c_name'=>'modular','c_array'=>\App\Components\Project\ProjectTable::TABLE_CN_ARR,'c_value'=>Illuminate\Support\Str::snake($con_arr['modular'])])

                        @include('component.grid.select',['c_title'=>'操作','c_name'=>'opt','c_array'=>$opt_list,'c_value'=>$con_arr['opt']])

                        {{--提交按钮--}}
                        @include('component.grid.submit')
                       </div>
                   </form>
               </div>

               <div class="layui-card-body">
                   <div>
                       <table class="layui-table text-c" style="width: 100%;">
                           <thead>
                           <th scope="col" colspan="100">
                               <span>共有<strong>{{$datas->total()}}</strong> 条数据</span>
                           </th>
                           <tr>
                               <th class="text-c" width="20">ID</th>
                               <th class="text-c" width="20">操作人</th>
                               <th class="text-c" width="20">模块</th>
                               <th class="text-c" width="80">操作功能</th>
                               <th class="text-c" width="100">操作时间</th>
                               <th class="text-c" width="100">操作</th>
                           </tr>
                           </thead>
                           <tbody>
                           @foreach($datas as $data)
                               <tr>
                                   <td>{{$data->id}}</td>
                                   <td>{{isset($data->admin->nick_name)?$data->admin->nick_name:''}}</td>
                                   <td>{{$data->modular_str}}</td>
                                   <td>{{$data->opt_str}}</td>
                                   <td>{{$data->created_at}}</td>
                                   <td>
                                       <button class="layui-btn layui-btn-sm" type="button"
                                               onclick="openPage('查看','{{URL::asset('admin/'.$modular.'/info')}}?id={{$data->id}}')">
                                           查看
                                       </button>
                                   </td>
                               </tr>
                           @endforeach
                           </tbody>
                       </table>
                   </div>
                   <div class="">
                       <div class="">
                           {{ $datas->appends($con_arr)->links() }}
                       </div>
                   </div>
               </div>
           </div>
       </div>

   @endsection

   @section('script')
       @include('component.grid.init')
       <script type="text/javascript">
           /*
            * 页面刷新
            *
            * By ldawn
            *
            */
           function refresh() {
               $('#search_form')[0].reset();
               location.replace('{{URL::asset('/admin/'.$modular.'/index')}}');
           }
       </script>
   @endsection