@extends('admin.layouts.app')

@section('content')
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-header">详情</div>
                <div class="layui-card-body" pad15>
                    <div class="layui-form">
                        @include('component.form.text',['c_title'=>'id','c_value'=>$data->id])
                        @include('component.form.text',['c_title'=>'操作人','c_value'=>isset($data->admin->nick_name)?$data->admin->nick_name:''])
                        @include('component.form.text',['c_title'=>'模块','c_value'=>$data->modular_str])
                        @include('component.form.text',['c_title'=>'操作','c_value'=>$data->opt_str])
                        @include('component.form.text',['c_title'=>'操作时间','c_value'=>$data->created_at])
                        @include('component.form.text_json',['c_title'=>'操作记录','c_value'=>$data->opt_value])
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@include('component.form.init',['form_url'=>'','c_no_img'=>1])

<script type="text/javascript">

</script>
