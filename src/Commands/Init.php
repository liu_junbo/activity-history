<?php

namespace Ldawn\ActivityHistory\Commands;

use Illuminate\Console\Command;
use Ldawn\Base\Store\BaseStore;

class Init extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'activity-history:init
     {--p=admin : 平台}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (config('app.env') == 'local') {
            BaseStore::createProjectTable();
            BaseStore::resetConfig('activity-history');
        }
    }
}
